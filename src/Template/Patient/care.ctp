<?php echo $this->element('medicalRecordsSidebar');?>

<div class="summaryContainer">
  <h5>Prontuário</h5>

  <div class="summary">
    <div class="">
      <h6>Jouber Gomes da Silva Santos</h6>
      <div class="patientData">
        <p>Idade: <span>30 anos</span></p>
        <p>Primeira consulta: <span>01/08/2018</span></p>
        <p>Atendimentos: <span>03</span></p>
      </div>
    </div>
    <div class="">
      <?=$this->Form->button('Imprimir atendimento')?>
    </div>
  </div>

  <div class="grayHighlight">
    <div class="medicalRecords">
      <div style="display:flex; align-items: center; padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Form->control('DATA', ['value'=>'12/10/2018']); ?>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Exames', '#'); ?>
        <div style="display:flex; align-items: center;">
          <?= $this->Form->control('Exame'); ?>
          <?= $this->Form->control('Quantidade'); ?>
          <?=$this->Html->link('Adicionar', '#');?>
        </div>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Prescrições', '#'); ?>
        <div style="display:flex; align-items: center;">
          <?= $this->Form->control('Medicamento'); ?>
          <?= $this->Form->control('Quantidade'); ?>
          <?=$this->Html->link('Adicionar', '#');?>
        </div>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Atestados', '#'); ?>
        <div style="display:flex; align-items: center;">
          <textarea name="name" rows="8" cols="80" style="border-radius: 4px;"></textarea>
        </div>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Observações médicas', '#'); ?>
        <div style="display:flex; align-items: center;">
          <textarea name="name" rows="8" cols="80" style="border-radius: 4px;"></textarea>
        </div>
      </div>
      <div class="saveRecords">
        <?=$this->Form->button('Imprimir')?>
        <?=$this->Form->button('Salvar')?>
      </div>
    </div>
  </div>
</div>
