<?php echo $this->element('registerSidebar');?>

<div class="registerContainer">
  <h5>Novo paciente</h5>

  <div class="grayHighlight">
    <div class="medicalRecords">
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Geral', '#'); ?>
        <div style="">
          <?= $this->Form->control('Nome completo*'); ?>
          <?= $this->Form->control('Data de nascimento*', ['placeholder'=>'__/__/__']); ?>
          <label for="">Sexo</label>
          <div style="display:flex; align-items: center;">
            <?= $this->Form->radio('Sexo', ['Masculino','Feminio']); ?>
          </div>
          <?= $this->Form->control('E-mail'); ?>
        </div>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Telefone', '#'); ?>
        <div style="display: flex; align-items: center:">
          <?= $this->Form->control('Celular*', ['placeholder'=>'(__) _____-____']); ?>
          <?= $this->Form->control('Fixo*', ['placeholder'=>'(__) ____-____']); ?>
        </div>
      </div>
      <div style="padding: 10px; border-bottom: solid 1px #dddddd">
        <?= $this->Html->link('Endereço', '#'); ?>
        <?= $this->Form->control('CEP', ['placeholder'=>'_____-___']); ?>
        <div style="display: flex; align-items: center:">
          <?= $this->Form->control('Endereço'); ?>
          <?= $this->Form->control('Numero'); ?>
        </div>
        <div style="display: flex; align-items: center:">
          <?= $this->Form->control('completo'); ?>
          <?= $this->Form->control('Bairro'); ?>
        </div>
        <div style="display: flex; align-items: center:">
          <?= $this->Form->control('Cidade'); ?>
          <div class="">
            <label for="">Estado</label>
            <?= $this->Form->select('Estado',['RJ']); ?>
          </div>
        </div>
      </div>
      <div class="saveRecords">
        <?=$this->Form->button('Imprimir')?>
        <?=$this->Form->button('Salvar')?>
      </div>
    </div>
  </div>
</div>
