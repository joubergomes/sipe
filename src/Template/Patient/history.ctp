<div class="filter">
  <?=$this->Form->control('filterName', ['label'=>false,'placeholder'=>'Digite o periodo inicial']);?>
  <?=$this->Form->control('filterDocument', ['label'=>false,'placeholder'=>'Digite o periodo final']);?>
  <?=$this->Form->button('Filtrar')?>
</div>

<div class="grayHighlight">
  <div class="medicalRecords">
    <p>------MEDICAMENTOS-----------------------------------------------------------------------------------------------</br>
    08-08-2018 </br>
    &nbsp; aplique 1 ML DIPIRONA SODICA 2MG 12/12 Hora(s) intravenosa</br>
    </br>
    ------CONTROLE DE PRESCRIÇÕES---------------------------------------------------------------------------</br>
    08-08-2018 </br>
    &nbsp; aplique 1 ML DIPIRONA SODICA 2MG 12/12 Hora(s) intravenosa</br>
    &nbsp; &nbsp; &nbsp; Precrição: 08-08-2018 06:30 </br>
    &nbsp; &nbsp; &nbsp; Precrição: 10-08-2018 12:30 </br>
    &nbsp; &nbsp; &nbsp; Precrição: 20-08-2018 05:50 </br>
    </br>
    ------ANOTAÇÕES MÉDICAS E DE ENFERMAGEM--------------------------------------------------------</br>
    08-08-2018 </br>
    &nbsp; Resumo do atendimento </br>
    &nbsp; &nbsp; &nbsp; Paciente apatico, apresentando febre e dores no corpo.
  </p>
  </div>
</div>
