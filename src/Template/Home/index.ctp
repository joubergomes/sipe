<div class="filter">
  <?=$this->Form->control('filterName', ['label'=>false,'placeholder'=>'Digite o nome do paciente']);?>
  <?=$this->Form->control('filterDocument', ['label'=>false,'placeholder'=>'Digite o CPF do paciente']);?>
  <?=$this->Form->button('Filtrar')?>
</div>

<div class="patientsContainer">
  <table class="table">
    <thead>
      <tr>
        <th style="width: 10%;">Id</th>
        <th style="width: 35%;">Nome do paciente</th>
        <th style="width: 15%; text-align: center;">CPF</th>
        <th style="width: 16%; text-align: center;">Último atendimento</th>
        <th style="text-align: center;">Ações</th>
      </tr>
    </thead>
    <tbody>
          <tr>
              <td>01</td>
              <td>Jouber Gomes da Silva Santos</td>
              <td style="text-align: center;">006.458.268-65</td>
              <td style="text-align: center;">15/09/2018</td>
              <td style="text-align: center;">
                <?= $this->Html->link('Prontuário', ['controller'=>'Patient', 'action'=>'care']);?>
                <span> | </span>
                <?= $this->Html->link('Histórico', ['controller'=>'Patient', 'action'=>'history']);?>
                <span> | </span>
                <?= $this->Html->link('Cadastro', '#');?>
              </td>
          </tr>
          <tr>
              <td>02</td>
              <td>Bruna Almenida do Amaral</td>
              <td style="text-align: center;">006.458.268-65</td>
              <td style="text-align: center;">16/09/2018</td>
              <td style="text-align: center;">
                <?= $this->Html->link('Prontuário', '#');?>
                <span> | </span>
                <?= $this->Html->link('Histórico', '#');?>
                <span> | </span>
                <?= $this->Html->link('Cadastro', '#');?>
              </td>
          </tr>
          <tr>
              <td>03</td>
              <td>Leni Gomes da Silva</td>
              <td style="text-align: center;">006.458.268-65</td>
              <td style="text-align: center;">25/09/2018</td>
              <td style="text-align: center;">
                <?= $this->Html->link('Prontuário', '#');?>
                <span> | </span>
                <?= $this->Html->link('Histórico', '#');?>
                <span> | </span>
                <?= $this->Html->link('Cadastro', '#');?>
              </td>
          </tr>
    </tbody>
  </table>
</div>
